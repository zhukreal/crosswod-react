import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'
import { Provider } from 'react-redux'
import App from './App'
import $ from 'jquery';
window.$ = $;

const history = createHistory()

// const store = createStore(
//   rootReducer,
//   composeWithDevTools(applyMiddleware(thunk))
// )

// if(localStorage.RocketToken){
//   store.dispatch(userLoggedIn(localStorage.RocketToken))
// }

ReactDOM.render(
    <Router history={history}>
      <Provider>
        <App />
      </Provider>
    </Router>,
document.getElementById('root'));

