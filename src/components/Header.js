import React from 'react'
import logo from './../img/logo.png'


export default class Header extends React.Component {
    constructor (props){
        super()
        this.state = {
            history: props.history,
            props: props,
            club: {},
            loaded: false
        }

    }

    componentDidMount(){

    }


    render() {
        return  (
            <div className="c-header clearfix">
                <div className="ch-left">
                    <img src={logo} alt=""/>
                </div>
                <div className="ch-right">
                    <div className="chr-1">CrossWOD</div>
                    <div className="chr-2">Кроссфит на ладони</div>
                    <div className="chr-3"><a href="https://crosswod.ru/">crosswod.ru</a></div>
                </div>
            </div>
        )
    }
}
